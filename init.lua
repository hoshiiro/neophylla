--
--  ███╗   ██╗███████╗ ██████╗ ██████╗ ██╗  ██╗██╗   ██╗██╗     ██╗      █████╗
--  ████╗  ██║██╔════╝██╔═══██╗██╔══██╗██║  ██║╚██╗ ██╔╝██║     ██║     ██╔══██╗
--  ██╔██╗ ██║█████╗  ██║   ██║██████╔╝███████║ ╚████╔╝ ██║     ██║     ███████║
--  ██║╚██╗██║██╔══╝  ██║   ██║██╔═══╝ ██╔══██║  ╚██╔╝  ██║     ██║     ██╔══██║
--  ██║ ╚████║███████╗╚██████╔╝██║     ██║  ██║   ██║   ███████╗███████╗██║  ██║
--  ╚═╝  ╚═══╝╚══════╝ ╚═════╝ ╚═╝     ╚═╝  ╚═╝   ╚═╝   ╚══════╝╚══════╝╚═╝  ╚═╝
--
--  Optimized for termux (may feel weird in other OS).

local o = vim.opt
local g = vim.g

o.shortmess = 'filnxtToOFI'

g.netrw_banner = 0

g.mapleader = ' '
g.maplocalleader = '.'

g.loaded_python3_provider = false
g.loaded_node_provider = false
g.loaded_perl_provider = false
g.loaded_ruby_provider = false

o.shiftwidth = 2
o.tabstop = 2
o.expandtab = true
o.smartindent = true

-- o.scrolloff = 20
o.termguicolors = true
o.number = true
-- o.relativenumber = true
o.cursorline = true

o.signcolumn = 'no'
o.ls = 0

require('keymaps')

--   _
--  | |    __ _ _____   _
--  | |   / _` |_  / | | |
--  | |__| (_| |/ /| |_| |
--  |_____\__,_/___|\__, |
--                  |___/

local used_lang = {
	'lua',
	'go',
	'python',
	'typescript',
	'javascript',
	'html',
	'css',
	'json',
	'yaml',
}
local lazy_spec = {
	{ 'nvim-tree/nvim-web-devicons' },
	{ 'nvim-lua/plenary.nvim' },
	{
		'folke/tokyonight.nvim',
		lazy = false,
		priority = 1000,
		config = function()
			require('config.tokyo')
		end,
	},
	{
		'nvim-treesitter/nvim-treesitter',
		event = { 'BufReadPost' },
		dependencies = {
			'windwp/nvim-ts-autotag',
		},
		build = ':TSUpdate',
		cnfig = function()
			require('nvim-treesitter.configs').setup({
				auto_install = true,
				highlight = { enable = true },
				indent = { enable = true },
				autotag = { enable = true },
			})
		end,
	},
	{
		'neovim/nvim-lspconfig',
		ft = used_lang,
		config = function()
			require('config.lspconfig')
		end,
	},
	{
		'hrsh7th/nvim-cmp',
		event = { 'InsertEnter' },
		dependencies = {
			'hrsh7th/cmp-path',
			'hrsh7th/cmp-buffer',
			'hrsh7th/cmp-nvim-lsp',

			'onsails/lspkind.nvim',

			'L3MON4D3/LuaSnip',
			'saadparwaiz1/cmp_luasnip',
		},
		config = function()
			require('config.cmp')
		end,
	},
	{
		'nvim-telescope/telescope.nvim',
		cmd = 'Telescope',
		dependencies = {
			{
				'nvim-telescope/telescope-fzf-native.nvim',
				build = 'make',
			},
		},
		branch = '0.1.x',
		config = function()
			local tscope = require('telescope')

			tscope.setup({
				defaults = {
					color_devicons = false,
					prompt_prefix = '   ',
					selection_caret = ' ',
					entry_prefix = ' ',
					layout_config = { width = 0.92, height = 0.60 },
				},
			})

			tscope.load_extension('fzf')
		end,
	},
	{
		'b0o/incline.nvim',
		ft = used_lang,
		config = function()
			require('config.incline')
		end,
	},
	{
		'stevearc/conform.nvim',
		ft = used_lang,
		opts = {
			formatters_by_ft = { lua = { 'stylua' } },
			format_on_save = {
				timeout_ms = 500,
				lsp_format = 'fallback',
			},
		},
	},
	{
		'echasnovski/mini.indentscope',
		ft = used_lang,
		version = false,
		opts = {},
	},
	{
		'windwp/nvim-autopairs',
		event = 'InsertEnter',
		opts = { check_ts = true, map_cr = true },
	},
	{
		'stevearc/oil.nvim',
		opts = {
			columns = {},
			float = {
				padding = 2,
				max_width = 92,
				max_height = 20,
			},
		},
	},
	{
		'NeogitOrg/neogit',
		cmd = { 'Neogit' },
		opts = {
			commit_view = {
				kind = 'tab',
			},
		},
	},
	{
		'wakatime/vim-wakatime',
		event = { 'BufRead', 'BufNewFile' },
	},
	{
		'kylechui/nvim-surround',
		event = { 'BufRead', 'BufNewFile' },
		opts = {},
	},
	{
		'preservim/vim-pencil',
		ft = { 'markdown', 'text', 'gemtext' },
		config = function()
			vim.o.conceallevel = 2
			vim.g['pencil#wrapModeDefault'] = 'soft'
			vim.cmd([[call pencil#init()]])
		end,
	},
}

local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'
if not (vim.uv or vim.loop).fs_stat(lazypath) then
	local lazyrepo = 'https://github.com/folke/lazy.nvim.git'
	local out = vim.fn.system({
		'git',
		'clone',
		'--filter=blob:none',
		'--branch=stable',
		lazyrepo,
		lazypath,
	})
	if vim.v.shell_error ~= 0 then
		vim.api.nvim_echo({
			{ 'Failed to clone lazy.nvim:\n', 'ErrorMsg' },
			{ out, 'WarningMsg' },
			{ '\nPress any key to exit...' },
		}, true, {})
		vim.fn.getchar()
		os.exit(1)
	end
end
vim.opt.rtp:prepend(lazypath)
require('lazy').setup({
	spec = lazy_spec,
	defaults = { lazy = true },
	install = { colorscheme = { 'tokyonight' } },
	performance = {
		cache = {
			enabled = true,
		},
		reset_packpath = true,
		rtp = {
			reset = true,
			-- paths = {},
			disabled_plugins = {
				'gzip',
				'matchit',
				'matchparen',
				'netrwPlugin',
				'tarPlugin',
				'tohtml',
				'tutor',
				'zipPlugin',
				'spellfile',
			},
		},
	},
})
